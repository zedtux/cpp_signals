#include <iostream>
#include <boost/bind.hpp>
#include "emiter_noarg.h"
#include "emiter.h"

/**
 *  \brief Appelle Moi
 *  
 *  Function sans argument qui envoie un message écrit en dur
 */
void appelleMoi()
{
	std::cout << "Hello World depuis appelleMoi()" << std::endl;
}

/**
 *  \brief Dir
 *  
 *  Function recevant un message et qui l'écrit dans le terminal
 *
 *  \param message Message à afficher
 */
void dir( std::string message )
{
	std::cout << "dir(): " << message << std::endl;
}

int main(int argc, char * argv[])
{
	//~----------------------------------------------------------------
	//~ Premièrement, utilisons la version sans arguments de la classe.
	//~ 
	//~ Cette classe utilise un signal qui ne contient pas d'arguments,
	//~ donc il appelle seulement la fonction connecté
	//~----------------------------------------------------------------
	EmiterNoArgs	emiterNoArgs;
	
	//~ Connecte le signal 'EmiterNoArgs' à la fonction local appelleMoi()
	emiterNoArgs.connect( boost::bind( &appelleMoi ) );
	emiterNoArgs.startJob();
	
	//~----------------------------------------------------------------
	//~ Puis, utilisons l'autre version avec un argument.
	//~ 
	//~ Cette classe utilise un signal qui contient un argument,
	//~ qui sera utilisé par la fonction pour afficher son message
	//~----------------------------------------------------------------
	Emiter		emiter;
	
	//~ Connecte le signal 'EmiterNoArgs' à la fonction local dir()
	emiter.connect( boost::bind( &dir, _1 ) );
	emiter.startJob();
	
	return 0;
}
