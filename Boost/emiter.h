//~ This file is part of zedtux Tutorial
//~ 
//~ This is free software: you can redistribute it and/or modify
//~ it under the terms of the GNU General Public License as published by
//~ the Free Software Foundation, either version 3 of the License, or
//~ (at your option) any later version.

//~ This is distributed in the hope that it will be useful,
//~ but WITHOUT ANY WARRANTY; without even the implied warranty of
//~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//~ GNU General Public License for more details.
//~ 
//~ You should have received a copy of the GNU General Public License
//~ along with this tutorial.  If not, see <http://www.gnu.org/licenses/>.
//~ 
//~ --------------------------------------------------------------------
//~ 
//~ 
//~ Copyright 2009 zedtux
//~ 

#ifndef EMITER_H
#define EMITER_H

#include <boost/signals.hpp>

class Emiter
{
	public:
		typedef boost::signal<void (std::string)>	mySignalEmit;
		
		Emiter();
		~Emiter();
		
		/**
		 *  \brief Connect
		 *
		 *  Fonction pour connecter un demandeur à un signal
		 *
		 *  \param subscriber Adresse de la fonction à connecter
		 *
		 *  \return une instance boost::signals::connection, connecté au 'subscriber'
		 */
		boost::signals::connection	connect(mySignalEmit::slot_function_type subscriber);
		
		/**
		 *  \brief Start Job
		 *  
		 *  Démarre le travail: Emet le signal
		 */
		void				startJob();
		
	private:
		mySignalEmit			signalEmit;
};

#endif
