//~ This file is part of zedtux Tutorial
//~ 
//~ This is free software: you can redistribute it and/or modify
//~ it under the terms of the GNU General Public License as published by
//~ the Free Software Foundation, either version 3 of the License, or
//~ (at your option) any later version.

//~ This is distributed in the hope that it will be useful,
//~ but WITHOUT ANY WARRANTY; without even the implied warranty of
//~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//~ GNU General Public License for more details.
//~ 
//~ You should have received a copy of the GNU General Public License
//~ along with this tutorial.  If not, see <http://www.gnu.org/licenses/>.
//~ 
//~ --------------------------------------------------------------------
//~ 
//~ 
//~ Copyright 2009 zedtux
//~ 

#include "emiter_noarg.h"

/**
 *  \brief Constructeur par défaut
 */
EmiterNoArgs::EmiterNoArgs()
{
}

/**
 *  \brief Destructeur
 */
EmiterNoArgs::~EmiterNoArgs()
{
}

/**
 *  \brief Connect
 *
 *  Fonction pour connecter un demandeur à un signal
 *
 *  \return une instance boost::signals::connection, connecté au 'subscriber'
 */
EmiterNoArgs::mySignalEmit EmiterNoArgs::signalEmitor()
{
	return signalEmit;
}

/**
 *  \brief Start Job
 *  
 *  Démarre le travail: Emet le signal
 */
void EmiterNoArgs::startJob()
{
	//~ Exécute le signal 'signalEmit'
	signalEmit.emit();
}
