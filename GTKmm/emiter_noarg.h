//~ This file is part of zedtux Tutorial
//~ 
//~ This is free software: you can redistribute it and/or modify
//~ it under the terms of the GNU General Public License as published by
//~ the Free Software Foundation, either version 3 of the License, or
//~ (at your option) any later version.

//~ This is distributed in the hope that it will be useful,
//~ but WITHOUT ANY WARRANTY; without even the implied warranty of
//~ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//~ GNU General Public License for more details.
//~ 
//~ You should have received a copy of the GNU General Public License
//~ along with this tutorial.  If not, see <http://www.gnu.org/licenses/>.
//~ 
//~ --------------------------------------------------------------------
//~ 
//~ 
//~ Copyright 2009 zedtux
//~ 

#ifndef EMITER_NOARGS_H
#define EMITER_NOARGS_H

#include <sigc++/sigc++.h>

class EmiterNoArgs
{
	public:
		typedef sigc::signal<void>	mySignalEmit;
		
		EmiterNoArgs();
		~EmiterNoArgs();
		
		/**
		 *  \brief Connect
		 *
		 *  Fonction pour connecter un demandeur à un signal
		 *
		 *  \param subscriber Adresse de la fonction à connecter
		 *
		 *  \return une instance boost::signals::connection, connecté au 'subscriber'
		 */
		EmiterNoArgs::mySignalEmit	signalEmitor();
		
		/**
		 *  \brief Start Job
		 *  
		 *  Démarre le travail: Emet le signal
		 */
		void				startJob();
		
	protected:
		mySignalEmit			signalEmit;
};

#endif
